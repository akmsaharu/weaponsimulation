﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour {

	public static MainMenuManager _MainMenuManager { get; set; }

	public GameObject _loadingPanel;
	public GameObject[] _listOfWeapons;

	int _storeLoadSceneIndex;
	private void Awake()
	{
		_MainMenuManager = this;
	}

	void Start () {
		_loadingPanel.SetActive(false);
	}

	
	
	// Update is called once per frame
	void Update () {
		
	}


	public void ChangeScenes(int index){
		_storeLoadSceneIndex = index;
		_loadingPanel.SetActive(true);
		Invoke("Load", 0.5f);
	}
	
	void Load()
	{
		SceneManager.LoadScene(_storeLoadSceneIndex);
	}

}
