﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuddileSelectWeapons : MonoBehaviour
{
    public static MuddileSelectWeapons _MuddileSelectWeapons {get;set;}

    [Header("Don't Touch here")]
	public Transform[] _childButtons;
    // Start is called before the first frame update

    private void Awake() {
        _MuddileSelectWeapons = this;
    }

    void Start()
    {
        _childButtons = new Transform[transform.childCount];
		int i = 0;
		foreach(Transform t in transform){
			_childButtons[i++] = t;
		}
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Select(int _serialIndex){
		for(int i = 0; i < _childButtons.Length; i++){
			if(i ==_serialIndex){
				_childButtons[i].gameObject.SetActive(true);
			}else{
				_childButtons[i].gameObject.SetActive(false);
			}
		}
	}


}
