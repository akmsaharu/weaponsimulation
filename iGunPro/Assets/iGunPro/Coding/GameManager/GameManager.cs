﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;
using UnityEngine.UI;
public class GameManager : MonoBehaviour {

	public static GameManager _gameManager{get;set;}

	[Header("Camera Touch Rotation")]
	public Slider _rotaionSlider;
	public float _Rotspeed;
	public float _BackRotspeed;
	public GameObject _cameraParent;
	public float mouseHor;
	public float mouseVer;

	[Header("Fire Button")]
	public GameObject _FireButton;

	[Header("Reload Button")]
	public GameObject _ReloadButton;

	[Header("VIDEO")]
	public VideoPlayer _videoPlayer;
	public YoutubePlayer _youtubePlayer;

	[Header("Don't Touch Here")]
	[HideInInspector]public bool _isCameraTouchRotation;
	[HideInInspector] public bool _isGamePlay;
	[HideInInspector] public bool _isActiveGlobalRotationTouch;
	

	 public Weapon _weapon;

	//22-10-2020
	[Header("Zoom")]
	public Camera _camera;
	public float _max_Zoom;
	public float _min_Zoom;
	public float _zoomIndex;
	public float _zoominOut;
	public Image _zoomImage;
	public Sprite _zoomOutSprite;
	public Sprite _zoomInSprite;

	[Header("Weapons List Buttons")]
	public Transform _WeaponsListButtonContent;
	public Transform[] _WeaponsListButton;


	private void Awake() {
		_gameManager = this;
	}

	// Use this for initialization
	void Start () {
		_isGamePlay = true;
		_videoPlayer.Stop();
		_camera.fieldOfView = 60;
		_rotaionSlider.value = 15;

		_zoomIndex = 1;
		_zoominOut = 50;

		int _WeaponsListButtonContentIndex = 0;
		_WeaponsListButton = new Transform[_WeaponsListButtonContent.childCount];
		foreach(Transform t in _WeaponsListButtonContent)
		{
			_WeaponsListButton[_WeaponsListButtonContentIndex++] = t;
		}

		if (WeaponsTypeController._WeaponsTypeController)
		{
			if (_WeaponsListButton.Length > 0)
			{
				for (int i = 0; i < _WeaponsListButton.Length; i++)
				{
					//AssaultRifle............................................
					if (WeaponsTypeController._WeaponsTypeController._GUNTypes == GUNTypes.AssaultRifle)
					{
						if (_WeaponsListButton[i].GetComponent<WeaponsButtonSelection>()._ThisWeaponTypes != ThisWeaponTypes.AssaultRifle)
						{
							_WeaponsListButton[i].gameObject.SetActive(false);
						}
					}
					//Mortar............................................
					if (WeaponsTypeController._WeaponsTypeController._GUNTypes == GUNTypes.Mortar)
					{
						if (_WeaponsListButton[i].GetComponent<WeaponsButtonSelection>()._ThisWeaponTypes != ThisWeaponTypes.Mortar)
						{
							_WeaponsListButton[i].gameObject.SetActive(false);
						}
					}
					//Pistol............................................
					if (WeaponsTypeController._WeaponsTypeController._GUNTypes == GUNTypes.Pistol)
					{
						if (_WeaponsListButton[i].GetComponent<WeaponsButtonSelection>()._ThisWeaponTypes != ThisWeaponTypes.Pistol)
						{
							_WeaponsListButton[i].gameObject.SetActive(false);
						}
					}
					//RocketLauncher............................................
					if (WeaponsTypeController._WeaponsTypeController._GUNTypes == GUNTypes.RocketLauncher)
					{
						if (_WeaponsListButton[i].GetComponent<WeaponsButtonSelection>()._ThisWeaponTypes != ThisWeaponTypes.RocketLauncher)
						{
							_WeaponsListButton[i].gameObject.SetActive(false);
						}
					}
					//Tank............................................
					if (WeaponsTypeController._WeaponsTypeController._GUNTypes == GUNTypes.Tank)
					{
						if (_WeaponsListButton[i].GetComponent<WeaponsButtonSelection>()._ThisWeaponTypes != ThisWeaponTypes.Tank)
						{
							_WeaponsListButton[i].gameObject.SetActive(false);
						}
					}
				}
			}
		}

	}


	
	// Update is called once per frame
	void Update () {
		/*if(Input.touchCount > 0 && _isActiveGlobalRotationTouch == false){
				WeaponTouchRotation();
			}*/
		if(_isCameraTouchRotation == true){
			_Rotspeed = _rotaionSlider.value;
			if(Input.touchCount > 0){
				WeaponTouchRotation();
			}else{
				AutoRotation();
			}
			
			
			
		}else{
			BackPreRotation();
		}

		if(_ReloadButton.GetComponent<Button>().interactable == false && _FireButton.GetComponent<Button>().interactable == false){
			//_FireButton.GetComponent<Button>().interactable = true;
			_ReloadButton.GetComponent<Button>().interactable = true;
		}
	}

	public void FIRE()
	{
		if(_weapon != null)
		{
			_weapon.SHOT();
			print("AA");
		}
	}

	void WeaponTouchRotation(){
		if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved) {

			Vector2 getTouchMove = Input.GetTouch(0).deltaPosition;
			mouseHor += getTouchMove.y;
			mouseVer += getTouchMove.x;

		}
		Quaternion deltaRotation = Quaternion.Euler(mouseHor * 0.2f,mouseVer * 0.2f,0/*Vector3.up * mouseVer * 0.2f */);
		_cameraParent.transform.rotation = deltaRotation;
	}

	void AutoRotation(){
		_cameraParent.transform.Rotate(Vector3.up * _Rotspeed * Time.deltaTime);
	}

	void BackPreRotation(){
		Quaternion deltaRotation = Quaternion.Lerp(_cameraParent.transform.rotation,Quaternion.Euler(Vector3.zero),_BackRotspeed * Time.deltaTime);
		_cameraParent.transform.rotation = deltaRotation;
	}

	public void _isCameraTouchRotation_ON(){
		_isCameraTouchRotation = true;
	}

	public void _isCameraTouchRotation_OFF(){
		_isCameraTouchRotation = false;
	}

	public void GamePlayActive(){
		_isGamePlay = true;
	}
	public void GamePlayDeActive(){
		_isGamePlay = false;
	}

	public void ChangeScenes(int index){
		_isGamePlay = false;
		SceneManager.LoadScene(index);
		if (WeaponsTypeController._WeaponsTypeController)
		{
			Destroy(WeaponsTypeController._WeaponsTypeController.gameObject);
		}
	}

	public void VideoPlay(){
		_youtubePlayer.Play(_youtubePlayer.youtubeUrl);
	//	_videoPlayer.Play();
	}

	public void VideoStop(){
		_youtubePlayer.Stop();
		//_videoPlayer.Stop();
	}

	public void ZoomInOut(){
		if(_zoominOut < _min_Zoom){
			_zoomIndex = 1;
			_zoomImage.sprite = _zoomOutSprite;
		}
		if(_zoominOut > _max_Zoom){
			_zoomIndex = -1;
			_zoomImage.sprite = _zoomInSprite;
		}

		_zoominOut = _camera.fieldOfView + _zoomIndex;
		_camera.fieldOfView = _zoominOut;
	}

	public void Timer(GameObject _sliderObj){
		_sliderObj.SetActive(!_sliderObj.activeSelf);
	}

	public void OnUITouchForActiveRotation_IN(){
		_isActiveGlobalRotationTouch = true;
	}
	public void OnUITouchForActiveRotation_OUT(){
		_isActiveGlobalRotationTouch = false;
	}

}
