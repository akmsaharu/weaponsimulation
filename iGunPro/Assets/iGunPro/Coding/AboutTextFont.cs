﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SGDSOFT.FONT;
public class AboutTextFont : MonoBehaviour
{
    private Text _text;
    // Start is called before the first frame update
    void Start()
    {
        //Init
        _text = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if(FontController._bangla == true){
			_text.lineSpacing = 3f;
		}
		if(FontController._english == true){
			_text.lineSpacing = 1.5f;
		}
    }
}
