﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[System.Serializable]
public class Maggin{
	public bool _isLoadedFull;
	public int _maximumMaggin;
	public int _currentMaggin;

	public GameObject _ammoListParent;
	public GameObject[] _ammoList;
	public Text _ammo_Text;
}

[System.Serializable]
public enum WeaponsType{
	SingleShot,AutoShoot
}



[RequireComponent(typeof(AudioSource))]
public class Weapon : MonoBehaviour {

	[Header("Weapon Type")]
	public WeaponsType _WeaponsType;

	[Header("Maggin")]
	public Maggin _maggin;
	
	[Header("Sound")]
	public AudioClip _fireClip;
	public AudioClip _emptyClip;

	[Header("Mazzle Flash")]
	public GameObject _MazzleFlash;
	public GameObject _smook;

	[Header("Sell")]
	public bool _sellInstanceOff;
	public GameObject _sell;
	public Transform _SellSpawnPosition;

	[Header("Animation")]
	public Animator _anim;
	private AudioSource _source;
	bool _isEmptyClip;
	bool _isEmpty;
	


	//private bool _isFire;

	// Use this for initialization
	void Start () {
		_source = GetComponent<AudioSource>();
		_MazzleFlash.SetActive(false);
	//	GameManager._gameManager._ReloadButton.GetComponent<Button>().interactable = false;
	switch(_WeaponsType){
		case WeaponsType.SingleShot:
		//	GameManager._gameManager._FireButton.GetComponent<Button>().onClick.AddListener(Fire);
		break;
		
	}
		
		GameManager._gameManager._ReloadButton.GetComponent<Button>().onClick.AddListener(RELOAD);
		GameManager._gameManager._FireButton.GetComponent<Button>().interactable = true;

	}

	public void SHOT()
	{
		switch (_WeaponsType)
		{
			case WeaponsType.SingleShot:
				Fire();
				break;
			case WeaponsType.AutoShoot:
				GameManager._gameManager._FireButton.GetComponent<Button>().interactable = true;

				if (_maggin._isLoadedFull == true)
				{


					//vibrate
					Handheld.Vibrate();

					//Fire
					_anim.SetBool("fire", true);


				}
				else
				{
					if (_isEmptyClip == false && _isEmpty == true)
					{
						StartCoroutine(PlayEmptySoundClip());
					}
				}
				break;
		}
	} 
	
	void Update(){

		if(GameManager._gameManager._isCameraTouchRotation == false){
			if( GameManager._gameManager._isGamePlay == true){
				UpdateMySelft();
			}
		}

	}

	// Update is called once per frame
	void UpdateMySelft () {

			
		//Fire();
		

	//	if(_maggin._isLoadedFull == false){
		
	//	}

		_maggin._ammo_Text.text = "" + _maggin._currentMaggin;

		if(_maggin._currentMaggin < _maggin._ammoList.Length && _maggin._isLoadedFull == true){
			
			_maggin._ammoListParent.SetActive(true);
			_maggin._ammoList[_maggin._currentMaggin].SetActive(false);
		}else{
			_maggin._ammoListParent.SetActive(false);
		}
		

	}

	public void RELOAD(){

		_anim.SetBool("reload",true);
		_isEmpty = false;
		GameManager._gameManager._FireButton.GetComponent<Button>().interactable = false;
		Invoke("TimeToReloadFalse",0.1f);
		
	}

	public void ReloadTrue(){
		/*if(Reload() == true){
			_maggin._isLoadedFull = true;
		}*/
		GameManager._gameManager._FireButton.GetComponent<Button>().interactable = false;
		GameManager._gameManager._ReloadButton.GetComponent<Button>().interactable = true;
		Invoke("TimeToReloadTrue",0.5f);
	}

	void TimeToReloadTrue(){
		if(Reload() == true){
			GameManager._gameManager._FireButton.GetComponent<Button>().interactable = true;
			_maggin._isLoadedFull = true;
		}
	}
	
	void TimeToReloadFalse(){
		_anim.SetBool("reload",false);
	}

	private bool Reload(){
		bool _isReload = false;

		if(_isReload == false){
			_maggin._currentMaggin = _maggin._maximumMaggin;
			for(int i = 0; i < _maggin._ammoList.Length; i++){
				_maggin._ammoList[i].SetActive(true);
			}
			//GameManager._gameManager._ReloadButton.GetComponent<Button>().interactable = false;
			_isReload = true;
		}

		return _isReload;
	
	}

	public void AutoFire(){
		switch(_WeaponsType){
			case WeaponsType.AutoShoot:
				/*GameManager._gameManager._FireButton.GetComponent<Button>().interactable = true;

					if(_maggin._isLoadedFull == true){


						//vibrate
						Handheld.Vibrate();

						//Fire
						_anim.SetBool("fire",true);
						
						
					}else{
						if(_isEmptyClip == false && _isEmpty == true){
							StartCoroutine(PlayEmptySoundClip());
						}
					}*/
				break;
		}
	}

	public void AutoFireStop(){
		_anim.SetBool("fire",false);
	}

	private void Fire(){
		if (Input.GetButtonUp("Fire1")){
		
		GameManager._gameManager._FireButton.GetComponent<Button>().interactable = true;

			if(_maggin._isLoadedFull == true){

			print("A");
				//vibrate
				Handheld.Vibrate();

				//Fire
				_anim.SetBool("fire",true);
				
				
			}else{
			print("B");
			if (_isEmptyClip == false && _isEmpty == true){
					StartCoroutine(PlayEmptySoundClip());
				}
			}
		}

	}

	IEnumerator PlayEmptySoundClip(){
		_source.PlayOneShot(_emptyClip,0.5f);
		_isEmptyClip = true;
		yield return new WaitForSeconds(0.5f);
		_isEmptyClip = false;
	}


	public void FireTrue(){

		_source.PlayOneShot(_fireClip,1);
		_MazzleFlash.SetActive(true);
		Instantiate(_smook,_MazzleFlash.transform.position,Quaternion.identity);
		_maggin._currentMaggin--;
		if (_sellInstanceOff == false)
		{
			Instantiate(_sell, _SellSpawnPosition.position, Quaternion.identity);
		}
		if(_maggin._currentMaggin <= 0){
			_isEmpty = true;
			GameManager._gameManager._FireButton.GetComponent<Button>().interactable = false;
			//GameManager._gameManager._ReloadButton.GetComponent<Button>().interactable = true;
			_maggin._isLoadedFull = false;
		}

		switch(_WeaponsType){
			case WeaponsType.SingleShot:
			_anim.SetBool("fire",false);
			break;
		}
		
		Invoke("TimeToDeactive",0.1f);

	}

	public void FireFalse(){
		switch(_WeaponsType){
			case WeaponsType.SingleShot:
			_anim.SetBool("fire",false);
			break;
		}
		
	}

	void TimeToDeactive(){
		_MazzleFlash.SetActive(false);
	}


	

}
