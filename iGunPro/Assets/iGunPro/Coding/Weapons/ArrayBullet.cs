﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArrayBullet : MonoBehaviour
{
    public Weapon _weapons;
    public GameObject _showAmmoArray;

    [Header("Don't Touch here")]
	public Transform[] _bullets;

    public bool _OneLoop_A;

    private void Start() {
        _bullets = new Transform[transform.childCount];
		int i = 0;
		foreach(Transform t in transform){
			_bullets[i++] = t;
		}
        GameManager._gameManager._FireButton.GetComponent<Button>().onClick.AddListener(SELL);
        GameManager._gameManager._ReloadButton.GetComponent<Button>().onClick.AddListener(OFF);
        _OneLoop_A = true;
    }

    private void Update() {
        
    }
    public void SELL () {

			
		//Fire();
		

	//	if(_maggin._isLoadedFull == false){
		
	//	}
        if(_OneLoop_A == true){
            if(_weapons._maggin._currentMaggin < _weapons._maggin._ammoList.Length && _weapons._maggin._isLoadedFull == true){
                _bullets[_weapons._maggin._currentMaggin].gameObject.SetActive(false);
                for(int i = _weapons._maggin._currentMaggin; i < _bullets.Length; i++){
                    _bullets[i].gameObject.SetActive(false);
                }
            }
        }

        
        
		
        
	}

    public void Reset(){
        for(int i = 0; i < _bullets.Length;i++){
            _bullets[i].gameObject.SetActive(true);
        }
        _showAmmoArray.SetActive(false);
        _OneLoop_A = true;
    }

    public void OFF(){
        _OneLoop_A = false;
        for(int i = 0; i < _bullets.Length;i++){
            _bullets[i].gameObject.SetActive(false);
        }
        Invoke("Reset",0.5f);
    }

}
