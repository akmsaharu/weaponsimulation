﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReloadSoundEffect : MonoBehaviour {

	public AudioSource _source;
	public AudioClip _loadIn;
	public AudioClip _loadOut;

	public void LOADIn(){
		_source.PlayOneShot(_loadIn,1);
	}
	public void LOADOut(){
		_source.PlayOneShot(_loadOut,1);
	}
}
