﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Smook : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Invoke("UpdateDestory",0.5f);
	}
	
	// Update is called once per frame
	void UpdateDestory () {
		Destroy(this.gameObject);
	}
}
