﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sell : MonoBehaviour {

	public float _forceUpSpeed;

	private Rigidbody _rigidbody;

	// Use this for initialization
	void Start () {
		_rigidbody = GetComponent<Rigidbody>();
		transform.rotation = Quaternion.Euler(0,0,Random.Range(-35f,35f));
		_rigidbody.AddForce(transform.up * _forceUpSpeed,ForceMode.Impulse);
		Invoke("UpdateGravity",0.2f);
	}
	
	// Update is called once per frame
	void UpdateGravity () {
		_rigidbody.useGravity = true;
		Invoke("TimeToDestory",1f);
	}

	void TimeToDestory(){
		Destroy(this.gameObject);
	}
}
