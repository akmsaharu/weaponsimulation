﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SGDSOFT.FONT;
public class FontChanger : MonoBehaviour {

	[Header("THis script doesn't support in TextMeshPro3D")]
	private Text _text;
	public int _fontSize_Bangla;
	public int _fontSize_English;
	[Header("Text")]
	public string _english_text;
	public string _bangla_text;

	private void Start() {
		_text = GetComponent<Text>();
	}

	// Update is called once per frame
	void Update () {
		
		if(FontController._bangla == true){
			_text.font = FontController._FontController._bangla_Font;
			_text.text = "" + _bangla_text;
			_text.fontSize = _fontSize_Bangla;
		}
		if(FontController._english == true){
			_text.font = FontController._FontController._english_Font;
			_text.text = "" + _english_text;
			_text.fontSize = _fontSize_English;
		}

	}
}
