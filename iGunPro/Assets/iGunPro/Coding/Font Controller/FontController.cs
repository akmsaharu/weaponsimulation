﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SGDSOFT{
	namespace FONT{
		public class FontController : MonoBehaviour {

			public static FontController _FontController {get;set;}
			public static bool _bangla;
			public static bool _english;

			[Header("Font Format")]
			public Font _bangla_Font;
			public Font _english_Font;

			[Header("Color")]
			public Color _normalColor;
			public Color _activeColor;

			[Header("Button")]
			public Image _banglaButton;
			public Image _englishButton;



			// Use this for initialization
			void Awake () {
				_FontController = this;
			}
			
			private void Start() {
				if(PlayerPrefs.GetInt("Ban") == 0){
					_english = true;
					_bangla = false;
				}else{
					_english = false;
					_bangla = true;
				}
			}

			// Update is called once per frame
			void Update () {
				if(_bangla == true){
					PlayerPrefs.SetInt("Ban",1);
					//button Image Color
					_banglaButton.color = _activeColor;
					_englishButton.color = _normalColor;
				}
				if(_english == true){
					PlayerPrefs.SetInt("Ban",0);
					//button Image Color
					_englishButton.color = _activeColor;
					_banglaButton.color = _normalColor;
				}
			}
			public void BANGLA(){
				_bangla = true;
				_english = false;
				
			}
			public void ENGLISH(){
				_bangla = false;
				_english = true;
				
			}
		}
	}
	

}
