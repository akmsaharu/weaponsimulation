﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum GUNTypes
{
    AssaultRifle,Pistol,RocketLauncher,Mortar, Tank
}

public class WeaponsTypeController : MonoBehaviour
{
    public static WeaponsTypeController _WeaponsTypeController { get; set; }

    public GUNTypes _GUNTypes;
    private void Awake()
	{
        _WeaponsTypeController = this;
    }

	// Start is called before the first frame update
	void Start()
    {
        DontDestroyOnLoad(this);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SelectWeaponType(int _index)
	{

	}
}
