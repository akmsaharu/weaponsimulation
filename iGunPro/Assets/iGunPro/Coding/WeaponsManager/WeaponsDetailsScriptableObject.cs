﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "SGDSOFT/Weapons/WeaponsDetails",order = 1)]
public class WeaponsDetailsScriptableObject : ScriptableObject
{
        public string _weaponsName;
        public string _weaponsName_Bangla;
        public string _firingRange;
        public string _firingRange_bangla;
        public string _barralRange;
        public string _barralRange_bangla;
        public string _origin;
        public string _origin_bangla;
        [TextArea(0,50)]public string _about_English;
        [TextArea(0,50)]public string _about_Bangla_Unicode;
        public float _about_Page_Text_Height;
        public string _video_URL;
        public Sprite _infoSprite;
        public Sprite[] _SliderImageSprites;

        [Header("About")]
        public string _firingRange_A;
        public string _firingRange_A_bangla;
        public string _barralRange_A;
        public string _barralRange_A_bangla;
        public string _origin_A;
        public string _origin_A_bangla;
        public string _firingRange_B;
        public string _firingRange_B_bangla;
        public string _barralRange_B;
        public string _barralRange_B_bangla;
        public string _origin_B;
        public string _origin_B_bangla;

}

