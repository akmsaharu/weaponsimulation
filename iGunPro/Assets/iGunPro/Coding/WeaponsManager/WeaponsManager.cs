﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using System;

[System.Serializable]
public class OutPutWeaponsDetails{
    public Text _weaponsName;
    public Slider _firingRange_Slider;
    public Text  _firingRange_Text;
    public Slider _barralRange_Slider;
    public Text _barralRange_Text;
    public Text _origin_Text;
    public Text _aboutText;
    public VideoPlayer _videoPlayer;
    public Image _infoImage;

}

public class WeaponsManager : MonoBehaviour
{
    public AboutPanel _AboutPanel;
    public YoutubePlayer _youtubePlayer;
    public List<OutPutWeaponsDetails> _OutPutWeaponsDetails;
    public List<WeaponsDetailsScriptableObject> _WeaponsDetailsScriptableObject;
    public List<Weapon> _weapons;

    // Start is called before the first frame update
    void Start()
    {
        SelectWeaponsButton(PlayerPrefs.GetInt("SelectedWeapons"));
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SelectWeaponsButton(int _serialIndex){
        for(int i = 0; i < _weapons.Count; i++){
            if(i == _serialIndex){

                GameManager._gameManager._weapon = _weapons[i].gameObject.GetComponent<Weapon>();
                _youtubePlayer.youtubeUrl = _WeaponsDetailsScriptableObject[i]._video_URL; 
                print(_WeaponsDetailsScriptableObject[i]._video_URL);
                //Setup Details
                _OutPutWeaponsDetails[i]._aboutText.GetComponent<FontChanger>()._english_text = "" + _WeaponsDetailsScriptableObject[i]._about_English;
                 _OutPutWeaponsDetails[i]._aboutText.GetComponent<FontChanger>()._bangla_text = "" + _WeaponsDetailsScriptableObject[i]._about_Bangla_Unicode;
                _OutPutWeaponsDetails[i]._aboutText.gameObject.GetComponent<LayoutElement>().preferredHeight = _WeaponsDetailsScriptableObject[i]._about_Page_Text_Height;
                //_OutPutWeaponsDetails[i]._barralRange_Slider.value = System.Int32.Parse(_WeaponsDetailsScriptableObject[i]._barralRange);
               
                _OutPutWeaponsDetails[i]._barralRange_Text.gameObject.GetComponent<FontChanger>()._english_text = _WeaponsDetailsScriptableObject[i]._barralRange;
                _OutPutWeaponsDetails[i]._barralRange_Text.gameObject.GetComponent<FontChanger>()._bangla_text = _WeaponsDetailsScriptableObject[i]._barralRange_bangla;

                _OutPutWeaponsDetails[i]._firingRange_Text.gameObject.GetComponent<FontChanger>()._english_text = _WeaponsDetailsScriptableObject[i]._firingRange;
                _OutPutWeaponsDetails[i]._firingRange_Text.gameObject.GetComponent<FontChanger>()._bangla_text = _WeaponsDetailsScriptableObject[i]._firingRange_bangla;
 
                _OutPutWeaponsDetails[i]._origin_Text.gameObject.GetComponent<FontChanger>()._english_text = _WeaponsDetailsScriptableObject[i]._origin;
                _OutPutWeaponsDetails[i]._origin_Text.gameObject.GetComponent<FontChanger>()._bangla_text = _WeaponsDetailsScriptableObject[i]._origin_bangla;


               // _OutPutWeaponsDetails[i]._barralRange_Text.text = "" + _WeaponsDetailsScriptableObject[i]._barralRange;
               // _OutPutWeaponsDetails[i]._firingRange_Text.text = "" + _WeaponsDetailsScriptableObject[i]._firingRange;
               
                //_OutPutWeaponsDetails[i]._weaponsName.text = "" + _WeaponsDetailsScriptableObject[i]._weaponsName;
               _OutPutWeaponsDetails[i]._weaponsName.gameObject.GetComponent<FontChanger>()._english_text = _WeaponsDetailsScriptableObject[i]._weaponsName;
                _OutPutWeaponsDetails[i]._weaponsName.gameObject.GetComponent<FontChanger>()._bangla_text = _WeaponsDetailsScriptableObject[i]._weaponsName_Bangla;

               // _OutPutWeaponsDetails[i]._firingRange_Slider.value = System.Int32.Parse(_WeaponsDetailsScriptableObject[i]._firingRange);
                _OutPutWeaponsDetails[i]._infoImage.sprite = _WeaponsDetailsScriptableObject[i]._infoSprite;
               
               // _OutPutWeaponsDetails[i]._origin_Text.text = _WeaponsDetailsScriptableObject[i]._origin;
               
//                _OutPutWeaponsDetails[i]._videoPlayer.url = _WeaponsDetailsScriptableObject[i]._video_URL;
                
                WeaponsImagesSlider._WeaponsImagesSlider._imgs_Sprite.Clear();
                for(int j = 0; j < _WeaponsDetailsScriptableObject[i]._SliderImageSprites.Length; j++){
                     WeaponsImagesSlider._WeaponsImagesSlider._imgs_Sprite.Add(_WeaponsDetailsScriptableObject[i]._SliderImageSprites[j]);
                }
                WeaponsImagesSlider._WeaponsImagesSlider.ChangesImage_Forward();

                //About Panel
                _AboutPanel._output[1]._firingRange_Text_OUT.gameObject.GetComponent<FontChanger>()._english_text = _WeaponsDetailsScriptableObject[i]._firingRange_A;
                _AboutPanel._output[1]._barralRange_Text_OUT.gameObject.GetComponent<FontChanger>()._english_text = _WeaponsDetailsScriptableObject[i]._barralRange_A;
                _AboutPanel._output[1]._origin_Text_OUT.gameObject.GetComponent<FontChanger>()._english_text = _WeaponsDetailsScriptableObject[i]._origin_A;

                _AboutPanel._output[1]._firingRange_Text_OUT.gameObject.GetComponent<FontChanger>()._bangla_text = _WeaponsDetailsScriptableObject[i]._firingRange_A_bangla;
                _AboutPanel._output[1]._barralRange_Text_OUT.gameObject.GetComponent<FontChanger>()._bangla_text = _WeaponsDetailsScriptableObject[i]._barralRange_A_bangla;
                _AboutPanel._output[1]._origin_Text_OUT.gameObject.GetComponent<FontChanger>()._bangla_text = _WeaponsDetailsScriptableObject[i]._origin_A_bangla;


                _AboutPanel._output[2]._firingRange_Text_OUT.gameObject.GetComponent<FontChanger>()._english_text = _WeaponsDetailsScriptableObject[i]._firingRange_B;
                _AboutPanel._output[2]._barralRange_Text_OUT.gameObject.GetComponent<FontChanger>()._english_text = _WeaponsDetailsScriptableObject[i]._barralRange_B;
                _AboutPanel._output[2]._origin_Text_OUT.gameObject.GetComponent<FontChanger>()._english_text = _WeaponsDetailsScriptableObject[i]._origin_B;

                _AboutPanel._output[2]._firingRange_Text_OUT.gameObject.GetComponent<FontChanger>()._bangla_text = _WeaponsDetailsScriptableObject[i]._firingRange_B_bangla;
                _AboutPanel._output[2]._barralRange_Text_OUT.gameObject.GetComponent<FontChanger>()._bangla_text = _WeaponsDetailsScriptableObject[i]._barralRange_B_bangla;
                _AboutPanel._output[2]._origin_Text_OUT.gameObject.GetComponent<FontChanger>()._bangla_text = _WeaponsDetailsScriptableObject[i]._origin_B_bangla;


                //Active Model;
                _weapons[i].gameObject.SetActive(true);
                _weapons[i].gameObject.GetComponent<Weapon>().RELOAD();
            }else{
                //DeActive Model;
                _weapons[i].gameObject.SetActive(false);
            }
        }
    }




}
