﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonWhiteHoverEffect : MonoBehaviour
{
   public void HoverByImage(GameObject _Image){
       _Image.SetActive(!_Image.activeSelf);
   }
}
