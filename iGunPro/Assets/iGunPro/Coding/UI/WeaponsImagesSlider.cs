﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class WeaponsImagesSlider : MonoBehaviour
{

    public static WeaponsImagesSlider _WeaponsImagesSlider {get;set;}

    public Image _outPutImage;
    [Header("Don't Touch")]
    public List<Sprite> _imgs_Sprite;
    public int _index;
    // Start is called before the first frame update
    void Awake()
    {  
        _WeaponsImagesSlider = this; 
        
    }
    

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangesImage_Forward(){
        _index++;
        if(_index > _imgs_Sprite.Count - 1){
            _index = 0;
        }
        _outPutImage.sprite = _imgs_Sprite[_index];
    }
    public void ChangesImage_Backward(){
        _index--;
        if(_index < 0){
            _index = _imgs_Sprite.Count - 1;
        }
        _outPutImage.sprite = _imgs_Sprite[_index];
    }

}
