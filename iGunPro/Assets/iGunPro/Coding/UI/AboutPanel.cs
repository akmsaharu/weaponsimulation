﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class OUTPUT{
    public Slider _firingRange_Slider_OUT;
    public Text  _firingRange_Text_OUT;
    public Slider _barralRange_Slider_OUT;
    public Text _barralRange_Text_OUT;
    public Text _origin_Text_OUT;
}
public class AboutPanel : MonoBehaviour
{
    [Header("Input")]
    public Text _weaponsName;
    public Slider _firingRange_Slider;
    public Text  _firingRange_Text;
    public Slider _barralRange_Slider;
    public Text _barralRange_Text;
    public Text _origin_Text;

    [Header("OutPut")]
     public Text _weaponsName_OUT;
    public List<OUTPUT> _output;


    // Update is called once per frame
    void Update()
    {
        _weaponsName_OUT.gameObject.GetComponent<FontChanger>()._english_text = "" + _weaponsName.gameObject.GetComponent<FontChanger>()._english_text;
        _weaponsName_OUT.gameObject.GetComponent<FontChanger>()._bangla_text = "" + _weaponsName.gameObject.GetComponent<FontChanger>()._bangla_text;

       // for(int i = 0; i < _output.Count; i++){
            //_output[i]._firingRange_Slider_OUT.value = _firingRange_Slider.value;
            _output[0]._firingRange_Text_OUT.gameObject.GetComponent<FontChanger>()._english_text = _firingRange_Text.gameObject.GetComponent<FontChanger>()._english_text;
            //_output[i]._barralRange_Slider_OUT.value = _barralRange_Slider.value;
            _output[0]._barralRange_Text_OUT.gameObject.GetComponent<FontChanger>()._english_text = _barralRange_Text.gameObject.GetComponent<FontChanger>()._english_text;
            _output[0]._origin_Text_OUT.gameObject.GetComponent<FontChanger>()._english_text = _origin_Text.gameObject.GetComponent<FontChanger>()._english_text;

            _output[0]._firingRange_Text_OUT.gameObject.GetComponent<FontChanger>()._bangla_text = _firingRange_Text.gameObject.GetComponent<FontChanger>()._bangla_text;
            _output[0]._barralRange_Text_OUT.gameObject.GetComponent<FontChanger>()._bangla_text = _barralRange_Text.gameObject.GetComponent<FontChanger>()._bangla_text;
            _output[0]._origin_Text_OUT.gameObject.GetComponent<FontChanger>()._bangla_text = _origin_Text.gameObject.GetComponent<FontChanger>()._bangla_text;
       // }
    }
}
