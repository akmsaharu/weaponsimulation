﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponsButtonSelectionContent : MonoBehaviour {

	public bool _isLeftContent;
	public bool _isMainMenuSelectWeapons;

	[Header("Don't Touch here")]
	//public Transform[] _parents;
	public Transform[] _childButtons;

	// Use this for initialization
	void Start () {
		/*int i = 0;
		for (int j = 0; j < _parents.Length; j++)	
		{
			_childButtons = new Transform[_parents[j].childCount];
			foreach (Transform t in transform)
			{
				_childButtons[i++] = t;
			}
		}*/

		if(_isLeftContent == true){
			//First Selected
			Select(0);
		}
	}
	
	public void LoadWeapon(int _weaponNumber)
	{
		PlayerPrefs.SetInt("SelectedWeapons", _weaponNumber);
	}
	public void AssaultRifle()
	{
		WeaponsTypeController._WeaponsTypeController._GUNTypes = GUNTypes.AssaultRifle;
	}
	public void Pistol()
	{
		WeaponsTypeController._WeaponsTypeController._GUNTypes = GUNTypes.Pistol;
	}
	public void RocketLauncher()
	{
		WeaponsTypeController._WeaponsTypeController._GUNTypes = GUNTypes.RocketLauncher;
	}
	public void Mortar()
	{
		WeaponsTypeController._WeaponsTypeController._GUNTypes = GUNTypes.Mortar;
	}
	public void Tank()
	{
		WeaponsTypeController._WeaponsTypeController._GUNTypes = GUNTypes.Tank;
	}


	public void Select(int _serialIndex){

		for(int i = 0; i < _childButtons.Length; i++){
			if(i ==_serialIndex){
				_childButtons[i].GetComponent<WeaponsButtonSelection>()._active = true;
			}else{
				_childButtons[i].GetComponent<WeaponsButtonSelection>()._active = false;
			}
		}

		
		foreach(GameObject g in MainMenuManager._MainMenuManager._listOfWeapons)
		{
			g.SetActive(false);
		}
		if (_serialIndex < MainMenuManager._MainMenuManager._listOfWeapons.Length)
		{
			MainMenuManager._MainMenuManager._listOfWeapons[_serialIndex].SetActive(true);
		}
		else
		{
			foreach (GameObject g in MainMenuManager._MainMenuManager._listOfWeapons)
			{
				g.SetActive(false);
			}
		}

		if (_isMainMenuSelectWeapons == true){
			MuddileSelectWeapons._MuddileSelectWeapons.Select(_serialIndex);
		}
	}
}
