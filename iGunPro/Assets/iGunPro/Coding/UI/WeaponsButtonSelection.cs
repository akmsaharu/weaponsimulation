﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public enum ThisWeaponTypes
{
	AssaultRifle, Pistol, RocketLauncher, Mortar ,Tank
}
public class WeaponsButtonSelection : MonoBehaviour {

	[Header("Weapon Type")]
	public ThisWeaponTypes _ThisWeaponTypes;

	public bool _active;

	[Header("Animation Image Object")]
	public GameObject _Active_Obj;
	public GameObject _Deactive_Obj;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(_active == true){
			_Active_Obj.SetActive(true);
			_Deactive_Obj.SetActive(false);
		}else{
			_Active_Obj.SetActive(false);
			_Deactive_Obj.SetActive(true);
		}
	}
}
