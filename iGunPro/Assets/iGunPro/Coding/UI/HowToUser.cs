﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SGDSOFT.FONT;

public class HowToUser : MonoBehaviour
{

    public Image _panel;
    public Sprite _English_Sprite;
     public Sprite _Bangla_Sprite;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(FontController._bangla == true){
			_panel.sprite = _Bangla_Sprite;
		}
		if(FontController._english == true){
			_panel.sprite = _English_Sprite;
		}
    }
}
